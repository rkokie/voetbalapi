<?php

require __DIR__ . '/Factory/VoetbalRankingFactory.php';
require __DIR__ . '/Factory/VoetbalScheduleFactory.php';
require __DIR__ . '/Factory/VoetbalResultFactory.php';
require __DIR__ . '/Factory/VoetbalMatchFactory.php';

class VoetbalAPI
{
    /** @var string */
    private $cookieFile = 'cookie.txt';
    private $loginPostfields = array('form_build_id', 'form_id', 'form_build_id');
    private $loginURL = 'https://www.voetbal.nl/inloggen';
    private $resultsURL = 'http://m.voetbal.nl/team/results/%d';
    private $scheduleURL = 'https://www.voetbal.nl/team/%s/programma/%s';
    private $rankingURL = 'https://www.voetbal.nl/team/%s/stand/%s';

    public function __construct()
    {
    }

    /**
     * @param string $username
     * @param string $password
     * @return void
     */
    public function authenticate($username, $password)
    {
        $html = $this->get($this->loginURL);

        $document = new DOMDocument();
        $document->loadHTML($html);
        $inputs = $document->getElementsByTagName('input');

        $postFields = array('email' => $username, 'password' => $password);
        for ($i1 = 0; $i1 < $inputs->length; $i1++) {
            $input = $inputs->item($i1);

            if (in_array($input->getAttribute('name'), $this->loginPostfields)) {
                $postFields[$input->getAttribute('name')] = $input->getAttribute('value');
            }
        }

        $this->post($this->loginURL, $postFields);
    }

    /**
     * @param string $teamId
     * @param string $type
     * @return VoetbalRanking[]
     */
    public function getRanking($teamId, $type = 'competitie')
    {
        $factory = new VoetbalRankingFactory();

        return $factory->createFromHTML(
            $this->get(sprintf($this->rankingURL, $teamId, $type))
        );
    }

    /**
     * @param string $teamId
     * @param string $type
     * @return VoetbalSchedule
     */
    public function getSchedule($teamId, $type = 'competitie')
    {
        $factory = new VoetbalScheduleFactory(
            new VoetbalMatchFactory()
        );

        return $factory->createFromHTML(
            $this->get(sprintf($this->scheduleURL, $teamId, $type))
        );
    }

    /**
     * @param int $teamId
     * @return VoetbalResults
     */
    public function getResults($teamId)
    {
        $factory = new VoetbalResultFactory(
            new VoetbalMatchFactory()
        );

        return $factory->createFromHTML(
            $this->get(sprintf($this->resultsURL, (int)$teamId))
        );
    }

    /**
     * @param string $url
     * @return string
     */
    private function get($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $body = curl_exec($ch);
        curl_close($ch);

        return $body;
    }

    /**
     * @param string $url
     * @param array $fields
     * @return string
     */
    private function post($url, $fields)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->arrayToQuery($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $body = curl_exec($ch);
        curl_close($ch);

        return $body;
    }

    /**
     * @param array $fields
     * @return string
     */
    private function arrayToQuery(array $fields)
    {
        $query = '';
        foreach ($fields as $key => $value) {
            $query .= $key . '=' . $value . '&';
        }
        rtrim($query, '&');

        return $query;
    }
}