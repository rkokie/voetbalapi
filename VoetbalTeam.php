<?php

class VoetbalTeam
{
    /** @var string */
    private $club;
    /** @var int */
    private $number;
    /** @var string */
    private $logo;

    /**
     * @param string $club
     * @param int $number
     * @param string $logo
     * @constructor
     */
    public function __construct($club, $number, $logo)
    {
        $this->club = (string)$club;
        $this->number = (int)$number;
        $this->logo = (string)$logo;
    }

    /**
     * @param string $string
     * @param string $logo
     * @return VoetbalTeam
     */
    public static function createFromString($string, $logo)
    {
        $parts = explode(' ', $string);
        $number = array_pop($parts);
        $club = implode(' ', $parts);

        return new self($club, $number, $logo);
    }

    /**
     * @return string
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %d', $this->getClub(), $this->getNumber());
    }
}
