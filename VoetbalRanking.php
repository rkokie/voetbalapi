<?php

class VoetbalRanking
{
    /** @var int */
    private $number;
    /** @var VoetbalTeam */
    private $team;
    /** @var int */
    private $played;
    /** @var int */
    private $won;
    /** @var int */
    private $tied;
    /** @var int */
    private $lost;
    /** @var int */
    private $points;
    /** @var int */
    private $goals;
    /** @var int */
    private $goalsAgainst;
    /** @var int */
    private $pointsReduction;

    /**
     * @param int $number
     * @param VoetbalTeam $team
     * @param int $played
     * @param int $won
     * @param int $tied
     * @param int $lost
     * @param int $points
     * @param int $goals
     * @param int $goalsAgainst
     * @param int $pointsReduction
     * @constructor
     */
    public function __construct(
        $number,
        VoetbalTeam $team,
        $played,
        $won,
        $tied,
        $lost,
        $points,
        $goals,
        $goalsAgainst,
        $pointsReduction
    )
    {
        $this->number = (int)$number;
        $this->team = $team;
        $this->played = (int)$played;
        $this->won = (int)$won;
        $this->tied = (int)$tied;
        $this->lost = (int)$lost;
        $this->points = (int)$points;
        $this->goals = (int)$goals;
        $this->goalsAgainst = (int)$goalsAgainst;
        $this->pointsReduction = (int)$pointsReduction;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @return int
     */
    public function getPlayed()
    {
        return $this->played;
    }

    /**
     * @return int
     */
    public function getWon()
    {
        return $this->won;
    }

    /**
     * @return int
     */
    public function getTied()
    {
        return $this->tied;
    }

    /**
     * @return int
     */
    public function getLost()
    {
        return $this->lost;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return int
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @return int
     */
    public function getGoalsAgainst()
    {
        return $this->goalsAgainst;
    }

    /**
     * @return int
     */
    public function getPointsReduction()
    {
        return $this->pointsReduction;
    }
}