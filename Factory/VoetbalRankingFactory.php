<?php

require __DIR__ . '/../VoetbalRanking.php';

class VoetbalRankingFactory
{
    /**
     * @param string $html
     * @return VoetbalRanking[]
     */
    public function createFromHTML($html)
    {
        $document = new DOMDocument();
        $document->loadHTML($html);

        $rows = $this->getTableContents($document);

        $rankings = array();
        // Start from 2 to skip the heading and -1 to skip to footer.
        for ($i1 = 2; $i1 < $rows->length - 1; $i1++) {
            $row = $rows->item($i1);
            $columns = $row->childNodes;

            $rowValues = array();
            for ($i2 = 0; $i2 < $columns->length; $i2++) {
                $column = $columns->item($i2);

                if ($i2 === 1) {
                    $rowValues[] = $this->getImageFromNode($column);
                } else {
                    $rowValues[] = trim($column->nodeValue);
                }
            }

            if (empty($rowValues)) {
                continue;
            }

            $rankings[] = new VoetbalRanking(
                $rowValues[0],
                VoetbalTeam::createFromString($rowValues[2], $rowValues[1]),
                $rowValues[3],
                $rowValues[4],
                $rowValues[5],
                $rowValues[6],
                $rowValues[7],
                $rowValues[8],
                $rowValues[9],
                $rowValues[10]
            );
        }

        return $rankings;
    }

    /**
     * @param DOMNode $node
     * @return null|string
     */
    private function getImageFromNode(DOMNode $node)
    {
        if ('div' !== $node->nodeName || !$node->hasChildNodes()) {
            return null;
        }

        try {
            return $node->childNodes->item(1)->childNodes->item(0)->getAttribute('src');
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param DOMDocument $document
     * @return DOMNodeList
     */
    private function getTableContents(DOMDocument $document)
    {
        $divs = $document->getElementsByTagName('div');
        for ($i = 0; $i < $divs->length; $i++) {
            $div = $divs->item($i);

            if ('table' === $div->getAttribute('class')) {
                return $div->childNodes;
            }
        }

        throw new RuntimeException('Could not find ranking table contents');
    }
}