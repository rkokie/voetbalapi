<?php

require __DIR__ . '/../VoetbalMatch.php';
require __DIR__ . '/../VoetbalTeam.php';

class VoetbalMatchFactory
{
    public function createFromString(\DateTime $date, $matchString)
    {
        $matchString = preg_replace('#\d{2}:\d{2}#', ' $0', $matchString);
        $matchString = preg_replace('#^\d{1,2} - \d{1,2}#', '$0 ', $matchString);

        $hasMatch = preg_match('#(\d{1,2}) - (\d{1,2}) (.+) (\d{1,2}) - (.+) (\d{1,2}) (\d{1,2}):(\d{1,2}) NR. (\d+)#', $matchString, $matches);

        if (!$hasMatch) {
            preg_match('#(.+) (\d{1,2}) - (.+) (\d{1,2}) (\d{1,2}):(\d{1,2}) NR. (\d+)#', $matchString, $matches);
            array_unshift($matches, null);
            array_unshift($matches, null);
            $goalsHome = null;
            $goalsGuest = null;
        } else {
            $goalsHome = $matches[1];
            $goalsGuest = $matches[2];
        }

        $homeTeam = new VoetbalTeam($matches[3], $matches[4]);
        $guestTeam = new VoetbalTeam($matches[5], $matches[6]);
        $hours = (int)$matches[7];
        $minutes = (int)$matches[8];
        $number = (int)$matches[9];

        $dateTime = clone $date;
        $dateTime->setTime($hours, $minutes);

        return new VoetbalMatch($number, $dateTime, $homeTeam, $guestTeam, $goalsHome, $goalsGuest);
    }
}