<?php

require __DIR__ . '/../VoetbalSchedule.php';

class VoetbalScheduleFactory
{
    /** @var VoetbalMatchFactory */
    private $matchFactory;

    private $dateStringConversions = array(
        'januari' => 'Januari',
        'februari' => 'Februari',
        'maart' => 'March',
        'april' => 'April',
        'mei' => 'May',
        'juni' => 'June',
        'juli' => 'Juli',
        'augustus' => 'August',
        'september' => 'September',
        'oktober' => 'October',
        'november' => 'November',
        'december' => 'December',
        'maandag' => 'Monday',
        'dinsdag' => 'Tuesday',
        'woensdag' => 'Wednesday',
        'donderdag' => 'Thursday',
        'vrijdag' => 'Friday',
        'zaterdag' => 'Saturday',
        'zondag' => 'Sunday',
    );

    /**
     * @param VoetbalMatchFactory $matchFactory
     * @constructor
     */
    public function __construct(VoetbalMatchFactory $matchFactory)
    {
        $this->matchFactory = $matchFactory;
    }

    /**
     * @param string $html
     * @return VoetbalSchedule
     */
    public function createFromHTML($html)
    {
        $dayContainers = $this->getDayContainers($html);
        $schedule = new VoetbalSchedule();

        foreach ($dayContainers as $dayContainer) {
            $dateTime = $this->getDateFromDayContainer($dayContainer);
            $matches = $this->getMatchesFromDayContainer($dayContainer, $dateTime);

            foreach ($matches as $match) {
                $schedule->addMatch($match);
            }
        }

        return $schedule;
    }

    /**
     * @param string $html
     * @throws \RuntimeException
     * @return DOMElement[]
     */
    private function getDayContainers($html)
    {
        $document = new DOMDocument();
        $document->loadHTML($html);

        $dayContainers = array();
        $divs = $document->getElementsByTagName('div');
        for ($i = 0; $i < $divs->length; $i++) {
            $div = $divs->item($i);

            if (!($div instanceof DOMElement)) {
                continue;
            }

            if ($div->hasAttribute('data-printable-target')) {
                $dayContainers[] = $div;
            }
        }

        return $dayContainers;
    }

    /**
     * @param DOMElement $dayContainer
     * @param DateTime $dateTime
     * @return array
     */
    private function getMatchesFromDayContainer(DOMElement $dayContainer, \DateTime $dateTime)
    {
        $matchNodes = $dayContainer->getElementsByTagName('a');
        $matches = array();

        for ($i = 0; $i < $matchNodes->length; $i++) {
            $matches[] = $this->getMatchFromMatchNode($matchNodes->item($i), $dateTime);
        }

        return $matches;
    }

    /**
     * @param DOMElement $matchNode
     * @param DateTime $dateTime
     * @return VoetbalMatch
     */
    private function getMatchFromMatchNode(DOMElement $matchNode, \DateTime $dateTime)
    {
        preg_match('/M\d+/', $matchNode->getAttribute('href'), $matches);
        $matchNumber = $matches[0];
        $homeTeam = trim($matchNode->childNodes->item(1)->childNodes->item(1)->nodeValue);
        $homeTeamLogo = $matchNode->childNodes->item(1)->childNodes->item(3)->childNodes->item(1)->getAttribute('src');
        $guestTeam = trim($matchNode->childNodes->item(5)->childNodes->item(3)->nodeValue);
        $guestTeamLogo = $matchNode->childNodes->item(5)->childNodes->item(1)->childNodes->item(1)->getAttribute('src');

        $matchTimeParts = explode(':', trim($matchNode->childNodes->item(3)->nodeValue));
        $matchDateTime = clone $dateTime;
        $matchDateTime->setTime((int)$matchTimeParts[0], (int)$matchTimeParts[1]);

        return new VoetbalMatch(
            $matchNumber,
            $matchDateTime,
            VoetbalTeam::createFromString($homeTeam, $homeTeamLogo),
            VoetbalTeam::createFromString($guestTeam, $guestTeamLogo)
        );
    }

    /**
     * @param DOMElement $dayContainer
     *
     * @return DateTime
     */
    private function getDateFromDayContainer(DOMElement $dayContainer)
    {
        $spans = $dayContainer->getElementsByTagName('span');

        for ($i = 0; $i < $spans->length; $i++) {
            $span = $spans->item($i);

            if (!($span instanceof DOMElement)) {
                continue;
            }

            if ('title' === $span->getAttribute('class')) {
                $dateString = trim($span->nodeValue);
                $englishDateString = str_replace(
                    array_keys($this->dateStringConversions),
                    array_values($this->dateStringConversions),
                    $dateString
                );

                $date = \DateTime::createFromFormat('l j F Y', $englishDateString)->setTime(0, 0);

                if ($date) {
                    return $date;
                }
            }
        }

        throw new \RuntimeException('Could not find date for scheduled day');
    }
}