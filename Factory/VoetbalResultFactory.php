<?php

require __DIR__ . '/../VoetbalResults.php';

class VoetbalResultFactory
{
    /** @var VoetbalMatchFactory */
    private $matchFactory;

    /**
     * @param VoetbalMatchFactory $matchFactory
     * @constructor
     */
    public function __construct(VoetbalMatchFactory $matchFactory)
    {
        $this->matchFactory = $matchFactory;
    }

    /**
     * @param string $html
     * @return VoetbalResults
     */
    public function createFromHTML($html)
    {
        $content = $this->getContentNodeFromHTML($html);
        $results = new VoetbalResults();

        $currentDate = new \DateTime();
        for ($i = 0; $i < $content->childNodes->length; $i++) {
            $nodeValue = $content->childNodes->item($i)->nodeValue;

            $date = \DateTime::createFromFormat('d-m-Y', $nodeValue);
            if ($date) {
                $currentDate = $date;
                continue;
            }

            $results->addMatch(
                $this->matchFactory->createFromString($currentDate, $nodeValue)
            );
        }

        return $results;
    }

    /**
     * @param string $html
     * @throws Exception
     * @return DOMElement
     */
    private function getContentNodeFromHTML($html)
    {
        $document = new DOMDocument();
        $document->loadHTML($html);

        $wrapperChildren = $document->getElementById('wrapper')->childNodes;
        for ($i = 0; $i < $wrapperChildren->length; $i++) {
            $childNode = $wrapperChildren->item($i);

            if (!($childNode instanceof DOMElement)) {
                continue;
            }

            if ('content' === $childNode->getAttribute('class')) {
                return $childNode;
            }
        }

        throw new \Exception('Could not find content node from html');
    }
}