PHP Class to retrieve data from the new voetbal.nl website.

##Methods:
- Retrieval of ranking
- Retrieval of schedule
- Retrieval of results

##Code example
```php
<?php
require __DIR__ . '/VoetbalAPI/VoetbalAPI.php';

$teamId = 0000;

$api = new VoetbalAPI();
$api->authenticate('username', 'password');

// Ranking
$ranking = $api->getRanking($teamId);

// Schedule
$ranking = $api->getSchedule($teamId);

// Results
$ranking = $api->getResults($teamId);
```
