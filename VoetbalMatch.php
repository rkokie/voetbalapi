<?php

class VoetbalMatch
{
    /** @var string */
    private $number;
    /** @var \DateTime */
    private $dateTime;
    /** @var VoetbalTeam */
    private $homeTeam;
    /** @var VoetbalTeam */
    private $guestTeam;
    /** @var int */
    private $homeGoals;
    /** @var int */
    private $guestGoals;

    /**
     * @param string $number
     * @param DateTime $dateTime
     * @param VoetbalTeam $home
     * @param VoetbalTeam $guest
     * @param int $goalsHome
     * @param int $goalsGuest
     * @constructor
     */
    public function __construct(
        $number,
        DateTime $dateTime,
        VoetbalTeam $home,
        VoetbalTeam $guest,
        $goalsHome = null,
        $goalsGuest = null
    )
    {
        $this->number = (string)$number;
        $this->dateTime = $dateTime;
        $this->homeTeam = $home;
        $this->guestTeam = $guest;

        if (null !== $goalsHome && null !== $goalsGuest) {
            $this->homeGoals = (int)$goalsHome;
            $this->guestGoals = (int)$goalsGuest;
        }
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @return VoetbalTeam
     */
    public function getHomeTeam()
    {
        return $this->homeTeam;
    }

    /**
     * @return VoetbalTeam
     */
    public function getGuestTeam()
    {
        return $this->guestTeam;
    }

    /**
     * @return int
     */
    public function getHomeGoals()
    {
        return $this->homeGoals;
    }

    /**
     * @return int
     */
    public function getGuestGoals()
    {
        return $this->guestGoals;
    }

    /**
     * @param string $teamName
     * @return bool
     */
    public function hasTeam($teamName)
    {
        return (string)$this->homeTeam === $teamName || (string)$this->guestTeam === $teamName;
    }
}