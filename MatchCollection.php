<?php

/**
 * @author Roger Kok <kok.roger@gmail.com>
 */
abstract class MatchCollection extends ArrayObject
{
    /**
     * @param VoetbalMatch[] $matches
     */
    public function __construct($matches = array())
    {
        parent::__construct($matches);
    }

    /**
     * @param VoetbalMatch $match
     * @return self
     */
    public function addMatch(VoetbalMatch $match)
    {
        $this->append($match);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function findMatchesForDate(\DateTime $date)
    {
        return new static(array_filter($this->getArrayCopy(), function ($match) use ($date) {
            /** @var VoetbalMatch $match */
            return $match->getDateTime()->diff($date)->format('%a') === '0';
        }));
    }

    /**
     * @inheritdoc
     */
    public function findMatchesForTeam($teamName)
    {
        return new static(array_filter($this->getArrayCopy(), function ($match) use ($teamName) {
            /** @var VoetbalMatch $match */
            return $match->hasTeam((string)$teamName);
        }));
    }
}